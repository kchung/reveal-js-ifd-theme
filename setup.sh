#!/bin/zsh

npm install
# remove git to avoid accidental commits
echo "removing git related files..."

rm -rf .git
rm -rf .gitignore

echo "Do you wish to install terminal feature?"

select yn in "Yes" "No"; do
    case $yn in
        Yes ) cd ifd-theme/terminal/client; npm install; cd ../server; npm install; cd ../../../; break;;
        No ) break;;
    esac
done

echo "Do you wish to install simple index.html?"

select yn in "Yes" "No"; do
    case $yn in
        Yes ) mv index_simple.html index.html; exit;;
        No ) rm -rf index_simple.html; exit;;
    esac
done

