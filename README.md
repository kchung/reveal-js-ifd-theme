# This is template to use Reveal.js for presentations.

Warning: the template is not yet completed.

## Dependency

- Since we are using [Vite](https://vitejs.dev/) as front-end and [Reveal.js](https://revealjs.com/) for the slides, [Node.js](https://nodejs.org/en/) version >=12.0.0 is required.

## How to use

Clone this repository to your local directory

```bash
git clone git@gitlab.ethz.ch:kchung/reveal-js-ifd-theme.git
```

Run shell script to install dependencies. This script will install all ```nodejs``` dependencies and remove git related files and folder to avoid accidental commits while you are making the presentation.
**Removing git related files/folders is for personal reason. You shouldn't do that if you clone this repo.**

```bash
chmod 777 setup.sh
./setup.sh
```

Edit index.html to create reveal.js slides

Then, start a local server to bundle and serve the presentation
```bash
npm run dev
```


Now, you can access to your slidese from your browser on localhost, port 3000 (localhost:3000)

## Integrate terminal inside of the slides

You can add terminal inside of the slides. To accomplish this, you need to install dependencies in `ifd-theme/terminal/client` and `ifd-theme/terminal/server` by running ```npm install```.

Then, before using terminal, please run the server side application using ```npm start```

