import 'reveal.js/dist/reveal.css'
import 'tailwindcss/tailwind.css'

import Reveal from 'reveal.js'

import './ifd-theme/theme.css'
import './style.css'

// Load reveal plugins
import RevealMath from 'reveal.js/plugin/math/math.js'
import RevealZoom from 'reveal.js/plugin/zoom/zoom.js'
import RevealMarkdown from 'reveal.js/plugin/markdown/markdown.js'
import RevealHighlight from 'reveal.js/plugin/highlight/highlight.js'
import RevealNotes from 'reveal.js/plugin/notes/notes.js'

const deck = new Reveal()
deck.initialize({ hash: true, slideNumber: true,
	// If mac, set height 1200. Else, 1080
  width: 1920, height: 1200, margin: 0, minScale: 1, maxScale: 1,
	pdfSeparateFragments: false,
	pdfMaxPagesPerSlide: 1,
  //math: {
  //  mathjax: 'https://cdn.jsdelivr.net/gh/mathjax/mathjax@2.7.8/MathJax.js',
  //},
  plugins: [RevealMath, RevealZoom, RevealMarkdown, RevealNotes, RevealHighlight]
})

// To add time in the slide
var d = new Date()
// get all element with id="dat"
var date = document.querySelectorAll("#dat");
var i;
// apply to all
for (i = 0; i < date.length; i++){
  date[i].innerHTML = d.toLocaleDateString();
}

